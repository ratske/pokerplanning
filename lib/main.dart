import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:pokerplanning/theme/style.dart';
import 'package:pokerplanning/ui/pages/home_page.dart';
import 'package:pokerplanning/ui/pages/login_page.dart';

Future<void> main() async {
  final FirebaseApp app = await FirebaseApp.configure(
    name: 'db2',
    options: const FirebaseOptions(
      googleAppID: '1:764191244222:android:ba1cddcf835c4224',
      apiKey: 'AIzaSyDD-gmdbfRxDIhKtCWXYXzTuumi7nMg4XI',
      databaseURL: 'https://pokerplanning-51a2d.firebaseio.com/',
    ),
  );

  final FirebaseAuth _auth = FirebaseAuth.instance;

  _auth.currentUser().then((user) {
    if (user != null)
      runApp(MaterialApp(
        title: "Poker Planning",
        home: HomePage(),
        theme: appTheme,
      ));
    else
      runApp(MaterialApp(
        title: "Poker Planning",
        home: LoginPage(
          app: app,
        ),
        theme: appTheme,
      ));
  }).catchError(() => runApp(MaterialApp(
        title: "Poker Planning",
        home: LoginPage(
          app: app,
        ),
        theme: appTheme,
      )));
}
