import 'dart:io';

enum authProblems { UserNotFound, PasswordNotValid, NetworkError }

class LoginErrorsHandler {
  authProblems checkMessages(String message) {
    authProblems errorType;
    if (Platform.isAndroid) {
      switch (message) {
        case 'There is no user record corresponding to this identifier. The user may have been deleted.':
          errorType = authProblems.UserNotFound;
          break;
        case 'The password is invalid or the user does not have a password.':
          errorType = authProblems.PasswordNotValid;
          break;
        case 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.':
          errorType = authProblems.NetworkError;
          break;
      }
    } else if (Platform.isIOS) {
      switch (message) {
        case 'Error 17011':
          errorType = authProblems.UserNotFound;
          break;
        case 'Error 17009':
          errorType = authProblems.PasswordNotValid;
          break;
        case 'Error 17020':
          errorType = authProblems.NetworkError;
          break;
      }
    }
    return errorType;
  }

  String showError(String message) {
    switch (checkMessages(message)) {
      case authProblems.UserNotFound:
        return "Usuário ou senha incorretos";
        break;
      case authProblems.PasswordNotValid:
        return "Usuário ou senha incorretos";
        break;
      default:
        return "";
    }
  }
}
