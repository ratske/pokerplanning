import 'package:flutter/material.dart';

/// Theme style configuration, this variable will be used at the very start
/// of the app
ThemeData appTheme = ThemeData(
    primaryColor: Color.fromRGBO(0, 121, 107, 1),
    primaryColorLight: Color.fromRGBO(72, 169, 153, 1),
    primaryColorDark: Color.fromRGBO(72, 169, 153, 1),
    accentColor: Color.fromRGBO(211, 47, 47, 1));

/// Secondary colors in three specific types
Color secondaryColor = Color.fromRGBO(211, 47, 47, 1);
Color secondaryColorLight = Color.fromRGBO(255, 102, 89, 1);
Color secondaryColorDark = Color.fromRGBO(154, 0, 7, 1);

/// Secondary colors in three specific types
Color textColor = Color.fromRGBO(255, 255, 255, 1);

/// Global pedding value
double globalPadding = 16.0;
