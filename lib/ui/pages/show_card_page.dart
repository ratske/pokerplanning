import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:pokerplanning/theme/style.dart';

class ShowCardPage extends StatefulWidget {
  ShowCardPage({this.cardNumber, this.app});
  final int cardNumber;
  final FirebaseApp app;

  @override
  _ShowCardPageState createState() => _ShowCardPageState();
}

class _ShowCardPageState extends State<ShowCardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Minha numeração é...")),
      body: Card(
        child: Padding(
          padding: EdgeInsets.all(globalPadding),
          child: Stack(
            children: <Widget>[
              Container(
                width: 200.0,
                height: 200.0,
              ),
              Container(
                alignment: new FractionalOffset(0.0, 0.0),
                decoration: new BoxDecoration(
                  border: new Border.all(
                    color: secondaryColor,
                    width: 50.0,
                  ),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Text(
                    widget.cardNumber.toString(),
                    style: TextStyle(fontSize: 100, color: secondaryColor),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
