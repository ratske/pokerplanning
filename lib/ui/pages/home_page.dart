import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:pokerplanning/helpers/sequences.dart';
import 'package:pokerplanning/theme/style.dart';
import 'package:pokerplanning/ui/pages/login_page.dart';
import 'package:pokerplanning/ui/pages/show_card_page.dart';
import 'package:pokerplanning/ui/widgets/sidemenu.dart';

class HomePage extends StatefulWidget {
  HomePage({this.app});
  final FirebaseApp app;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: widgetSideMenu,
      appBar: AppBar(
        title: Text("Poker Planning"),
        centerTitle: true,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              _openAlertDialog();
            },
            child:
                Image(image: AssetImage("images/icons/exit_to_app_white.png")),
          ),
        ],
      ),
      body: GridView(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
        padding: EdgeInsets.all(globalPadding),
        children: _cards(),
      ),
    );
  }

  /// List all the cards for the desired poker planning sequence
  List<Widget> _cards() {
    List<GestureDetector> cards = [];

    fibonacciSequence.forEach((f) {
      GestureDetector card = GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ShowCardPage(
                        cardNumber: f,
                      )));
        },
        child: Card(
          color: secondaryColor,
          child: Center(
            child: Text(
              f.toString(),
              style: TextStyle(color: textColor, fontSize: 25.0),
            ),
          ),
        ),
      );

      cards.add(card);
    });

    return cards;
  }

  Future<void> _openAlertDialog() async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text("Deseja fazer logout do aplicativo?"),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                _auth.signOut();
                Navigator.pop(context);
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
              child: Text("Sim"),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Não"),
            ),
          ],
        );
      },
    );
  }
}
