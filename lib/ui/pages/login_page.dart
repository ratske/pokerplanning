import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:pokerplanning/helpers/login_errors_handler.dart';
import 'package:pokerplanning/theme/style.dart';
import 'package:pokerplanning/ui/pages/home_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.app});
  final FirebaseApp app;
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  String _message = "";

  Future<FirebaseUser> _handleSignIn(String email, String password) async {
    FirebaseUser user = await _auth.signInWithEmailAndPassword(
        email: email, password: password);

    return user;
  }

  void _login() {
    _handleSignIn(_emailController.text, _passwordController.text)
        .then((FirebaseUser user) => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage())))
        .catchError((e) {
      LoginErrorsHandler loginErrorsHandler = LoginErrorsHandler();
      print(loginErrorsHandler.showError(e.message));
      setState(() {
        _message = loginErrorsHandler.showError(e.message);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Poker Planning"),
          centerTitle: true,
        ),
        body: Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: _formUI(),
        ));
  }

  Widget _formUI() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: _emailController,
            decoration: InputDecoration(labelText: "E-mail"),
            keyboardType: TextInputType.emailAddress,
            validator: validateEmail,
          ),
          TextFormField(
            controller: _passwordController,
            decoration: InputDecoration(labelText: "Senha"),
            obscureText: true,
            validator: validatePassword,
          ),
          Padding(
            padding: EdgeInsets.only(top: 16),
            child: SizedBox(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _login();
                  }
                },
                color: secondaryColor,
                child: Text(
                  "ENTRAR",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
          Text(
            _message,
            style: TextStyle(
              fontSize: 15.0,
              color: Colors.red,
            ),
          ),
          FlatButton(
            padding: EdgeInsets.all(16),
            child: Text("SE AINDA NÃO TEM CADASTRO, CLIQUE AQUI"),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? 'Digite um e-mail válido.' : null;
  }

  String validatePassword(String value) {
    return (value.isEmpty) ? 'Digite uma senha válida.' : null;
  }
}
